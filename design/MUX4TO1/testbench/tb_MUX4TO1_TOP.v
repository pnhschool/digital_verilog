/*====================================

     MODULE : tb_MUX4TO1_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_MUX4TO1_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter   D_W = 2;


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg[D_W-1:0]   I_DIN0; 
reg[D_W-1:0]   I_DIN1; 
reg[D_W-1:0]   I_DIN2; 
reg[D_W-1:0]   I_DIN3; 
reg[1:0]       I_SEL ; 
       
wire[D_W-1:0]  O_DOUT; 


//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
MUX4TO1_TOP #(2) uut0_MUX4TO1_TOP(
	.I_DIN0   (I_DIN0)   ,
	.I_DIN1   (I_DIN1)   ,
	.I_DIN2   (I_DIN2)   ,
	.I_DIN3   (I_DIN3)   ,
	.I_SEL    (I_SEL )   , 
                     
	.O_DOUT   (O_DOUT)     
);


//=====================================
//
//          STIMULUS
//
//=====================================
// input signal generation
initial begin
	$display("===== SIM START =====");
	// insert your code ...
	I_DIN0 = 2'b00;
	I_DIN1 = 2'b01;
	I_DIN2 = 2'b10;
	I_DIN3 = 2'b11;

	I_SEL  = 2'b00; #100;
	I_SEL  = 2'b01; #100;
	I_SEL  = 2'b10; #100;
	I_SEL  = 2'b11; #100;

	I_SEL  = 2'bzz;
	$display("=====  SIM END  =====");
	$finish;
end

endmodule


