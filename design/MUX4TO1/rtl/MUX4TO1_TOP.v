/*====================================

     MODULE : MUX4TO1_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module MUX4TO1_TOP(
	I_DIN0       ,
	I_DIN1       ,
	I_DIN2       ,
	I_DIN3       ,
	I_SEL        , 

	O_DOUT         
);


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter   D_W = 1;


//=====================================
//
//          I/O PORTS 
//
//=====================================
input[D_W-1:0]   I_DIN0  ;
input[D_W-1:0]   I_DIN1  ;
input[D_W-1:0]   I_DIN2  ;
input[D_W-1:0]   I_DIN3  ;
input[1:0]       I_SEL   ;

output[D_W-1:0]  O_DOUT  ;   
// for behavioral modeling
//reg[D_W-1:0]     O_DOUT  ; 


//=====================================
//
//          REGISTERS
//
//=====================================


//=====================================
//
//          WIRES
//
//=====================================


//=====================================
//
//          MAIN
//
//=====================================
// behavioral modeling
/*
always @(I_DIN0 or I_DIN1 or I_DIN2 or I_DIN3 or I_SEL)
	case(I_SEL)
	2'b00 : O_DOUT <= I_DIN0;
	2'b01 : O_DOUT <= I_DIN1;
	2'b10 : O_DOUT <= I_DIN2;
	2'b11 : O_DOUT <= I_DIN3;
	endcase
*/

assign     O_DOUT = (I_SEL == 2'b00) ? I_DIN0 : 
                    (I_SEL == 2'b01) ? I_DIN1 : 
                    (I_SEL == 2'b10) ? I_DIN2 : 
                                       I_DIN3 ; // I_SEL == 2'b11
endmodule


