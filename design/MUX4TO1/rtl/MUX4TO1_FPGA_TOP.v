/*====================================

     MODULE : MUX4TO1_FPGA_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module MUX4TO1_FPGA_TOP(
	I_DIN0       ,
	I_DIN1       ,
	I_DIN2       ,
	I_DIN3       ,
	I_SEL        , 

	O_DOUT         
);


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter   D_W = 2;


//=====================================
//
//          I/O PORTS 
//
//=====================================
input[D_W-1:0]   I_DIN0  ;
input[D_W-1:0]   I_DIN1  ;
input[D_W-1:0]   I_DIN2  ;
input[D_W-1:0]   I_DIN3  ;
input[1:0]       I_SEL   ;

output[D_W-1:0]  O_DOUT  ;   


//=====================================
//
//          REGISTERS
//
//=====================================


//=====================================
//
//          WIRES
//
//=====================================


//=====================================
//
//          MAIN
//
//=====================================
// MUX4TO1_TOP instance
MUX4TO1_TOP #(2) u_MUX4TO1_TOP (
	.I_DIN0  (I_DIN0)  ,
	.I_DIN1  (I_DIN1)  ,
	.I_DIN2  (I_DIN2)  ,
	.I_DIN3  (I_DIN3)  ,
	.I_SEL   (I_SEL )  , 
                    
	.O_DOUT  (O_DOUT)    
);


endmodule


