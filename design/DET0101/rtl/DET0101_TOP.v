/*====================================

     MODULE : DET0101_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module DET0101_TOP(
	nrst        ,
	clk         ,
    i_seq       ,
    o_detected  ,
);


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter    S_INIT = 2'b00;
parameter    S_A    = 2'b01;
parameter    S_B    = 2'b10;
parameter    S_C    = 2'b11;


//=====================================
//
//          I/O PORTS 
//
//=====================================
input        nrst       ; 
input        clk        ;
input        i_seq      ;
output       o_detected ;


//=====================================
//
//          REGISTERS
//
//=====================================
reg[1:0]     r_pstate   ; // state register


//=====================================
//
//          WIRES
//
//=====================================
reg[1:0]     w_nstate   ;
reg          w_detected ;

//=====================================
//
//          MAIN
//
//=====================================
// state register
always @(negedge nrst or posedge clk)
	if(~nrst) begin
		r_pstate      <= #1 S_INIT; 
	end
	else begin
		r_pstate      <= #1 w_nstate;
	end


// nstate
always @(r_pstate or i_seq) begin
	case(r_pstate)
	S_INIT : 
		if(~i_seq) begin
			w_nstate   <= S_A;
			w_detected <= 1'b0;
		end
		else begin
			w_nstate   <= S_INIT;
			w_detected <= 1'b0;
		end			
	S_A    :
		if(~i_seq) begin
			w_nstate   <= S_A;
			w_detected <= 1'b0;
		end
		else begin
			w_nstate   <= S_B;
			w_detected <= 1'b0;
		end
	S_B    :
		if(~i_seq) begin
			w_nstate   <= S_C;
			w_detected <= 1'b0;
		end
		else begin
			w_nstate   <= S_INIT;
			w_detected <= 1'b0;
		end
	S_C    :
		if(~i_seq) begin	
			w_nstate   <= S_A;
			w_detected <= 1'b0;
		end
		else begin
			w_nstate   <= S_B;
			w_detected <= 1'b1;
		end
	default :
	    begin
			w_nstate   <= S_INIT;
			w_detected <= 1'b0;
		end	
	endcase
end


//=====================================
//
//          OUTPUT ASSIGN
//
//=====================================
assign   o_detected = w_detected;

endmodule


