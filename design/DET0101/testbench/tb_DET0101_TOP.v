/*====================================

     MODULE : tb_DET0101_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_DET0101_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter HP_CLK = 50; // Half period of Clock


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg        nrst       ; 
reg        clk        ;
reg        i_seq      ;
wire       o_detected ;

wire[1:0]  w_r_pstate ;

//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
DET0101_TOP uut0_DET0101_TOP(
	.nrst       (nrst      ),
	.clk        (clk       ),
    .i_seq      (i_seq     ),
    .o_detected (o_detected)
);

assign     w_r_pstate = uut0_DET0101_TOP.r_pstate;


//=====================================
//
//          STIMULI
//
//=====================================
// clock generation
initial begin
	clk = 1'b0;
	forever clk = #(HP_CLK) ~clk;
end

// reset generation
initial begin
	nrst = 1'b1;
	repeat(20)@(posedge clk)
	@(negedge clk)
	nrst = 1'b0;
	repeat(2) @(negedge clk);
	nrst = 1'b1;
end

// input signal generation
integer i;
initial begin
	$display("===== SIM START =====");
	for(i=0;i<100;i=i+1) begin
		@(negedge clk)
		i_seq = $random % 2;	
	end
	$display("=====  SIM END  =====");
	$finish;
end

// monitoring
reg[8*8-1:0] pstate_str;
always @(posedge clk) begin
	// example
	//$display("[monitor] i_cnt_en=%u,o_cnt=%u @%6dns", i_cnt_en, o_cnt, $time); 
	case(uut0_DET0101_TOP.r_pstate)
	uut0_DET0101_TOP.S_INIT : pstate_str = "S_INIT";
	uut0_DET0101_TOP.S_A    : pstate_str = "S_A";
	uut0_DET0101_TOP.S_B    : pstate_str = "S_B";
	uut0_DET0101_TOP.S_C    : pstate_str = "S_C";
	endcase
	$display("[monitor] i_seq=%b,o_detected=%b,DET0101_TOP.r_pstate=%6s @%6dns", 
			 i_seq, o_detected, pstate_str, $time); 
end

endmodule


