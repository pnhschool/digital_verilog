/*====================================

     MODULE : CNT10_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module CNT10_TOP(
	nrst      ,
	clk       ,
	i_cnt_en  ,
	o_cnt            
);


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
input        nrst      ; 
input        clk       ;
input        i_cnt_en  ;
output[3:0]  o_cnt     ;


//=====================================
//
//          REGISTERS
//
//=====================================
reg[3:0]     r_cnt; //counter register


//=====================================
//
//          WIRES
//
//=====================================


//=====================================
//
//          MAIN
//
//=====================================
// counter register
always @(negedge nrst or posedge clk)
	if(~nrst) begin
		r_cnt          <= #1 4'd0;
	end
	else begin
		if(i_cnt_en) begin
			if(r_cnt == 4'd9)
				r_cnt  <= #1 4'd0;
			else
				r_cnt  <= #1 r_cnt + 4'd1;
		end
		else begin
			r_cnt      <= #1 r_cnt;
		end
	end


//=====================================
//
//          OUTPUT ASSIGN
//
//=====================================
assign      o_cnt = r_cnt;

endmodule


