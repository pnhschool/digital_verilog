/*====================================

     MODULE : tb_CNT10_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_CNT10_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter HP_CLK = 50; // Half period of Clock


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg        nrst     ; 
reg        clk      ;
reg        i_cnt_en ;
wire[3:0]  o_cnt    ;


//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
CNT10_TOP uut0_CNT10_TOP(
	.nrst     (nrst       ),
	.clk      (clk        ),
    .i_cnt_en (i_cnt_en   ),
    .o_cnt    (o_cnt      )     
);


//=====================================
//
//          STIMULI
//
//=====================================
// clock generation
initial begin
	clk = 1'b0;
	forever clk = #(HP_CLK) ~clk;
end

// reset generation
initial begin
	nrst = 1'b1;
	@(posedge clk)
	@(negedge clk)
	nrst = 1'b0;
	repeat(2) @(negedge clk);
	nrst = 1'b1;
end

// input signal generation
integer i;
initial begin
	$display("===== SIM START =====");
	i_cnt_en = 0;
	for(i=0;i<100;i=i+1) begin 
		repeat(5) @(negedge clk);
		i_cnt_en = ~i_cnt_en;
	end
	$display("=====  SIM END  =====");
	$finish;
end

// monitoring 
always @(posedge clk) begin
	// example
	$display("[monitor] i_cnt_en=%u,o_cnt=%u @%6dns", i_cnt_en, o_cnt, $time); 
end

endmodule


