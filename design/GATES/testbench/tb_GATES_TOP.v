/*====================================

     MODULE : tb_GATES_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_GATES_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter HP_CLK = 50; // Half period of Clock


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg	  I_X       ;
reg	  I_Y       ;
reg	  I_E       ;

wire  O_AND     ;
wire  O_OR      ;
wire  O_NOT     ;
wire  O_BUF     ;
wire  O_TBUF    ;
wire  O_NAND    ;
wire  O_NOR     ;
wire  O_XOR     ;
wire  O_XNOR    ;  

//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
GATES_TOP uut0_GATES_TOP(
	.I_X    (I_X    )   ,
	.I_Y    (I_Y    )   ,
	.I_E    (I_E    )   ,
                    
	.O_AND  (O_AND  )   ,
	.O_OR   (O_OR   )   ,
	.O_NOT  (O_NOT  )   ,
	.O_BUF  (O_BUF  )   ,
	.O_TBUF (O_TBUF )   ,
	.O_NAND (O_NAND )   ,
	.O_NOR  (O_NOR  )   ,
	.O_XOR  (O_XOR  )   ,
	.O_XNOR (O_XNOR )    
);


//=====================================
//
//          STIMULI
//
//=====================================
// input signal generation
initial begin
	$display("===== SIM START =====");
	// insert your code ...
	{I_X,I_Y,I_E} = 3'b000; #100;
	{I_X,I_Y,I_E} = 3'b001; #100;
	{I_X,I_Y,I_E} = 3'b010; #100;
	{I_X,I_Y,I_E} = 3'b011; #100;
	{I_X,I_Y,I_E} = 3'b100; #100;
	{I_X,I_Y,I_E} = 3'b101; #100;
	{I_X,I_Y,I_E} = 3'b110; #100;
	{I_X,I_Y,I_E} = 3'b111; #100;

	{I_X,I_Y,I_E} = 3'b000;

	$display("=====  SIM END  =====");
	$finish;
end

endmodule


