/*====================================

     MODULE : GATES_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module GATES_TOP(
	I_X       ,
	I_Y       ,
	I_E       ,

	O_AND     ,
	O_OR      ,
	O_NOT     ,
	O_BUF     ,
	O_TBUF    ,
	O_NAND    ,
	O_NOR     ,
	O_XOR     ,
	O_XNOR     
);


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
input	I_X       ;
input	I_Y       ;
input	I_E       ;

output  O_AND     ;
output  O_OR      ;
output  O_NOT     ;
output  O_BUF     ;
output  O_TBUF    ;
output  O_NAND    ;
output  O_NOR     ;
output  O_XOR     ;
output  O_XNOR    ;  

//=====================================
//
//          REGISTERS
//
//=====================================


//=====================================
//
//          WIRES
//
//=====================================


//=====================================
//
//          MAIN
//
//=====================================


//=====================================
//
//          OUTPUT ASSIGN
//
//=====================================
//assign    O_AND = I_X & I_Y;
and u_and2(O_AND, I_X, I_Y);
assign    O_OR    = I_X | I_Y;
assign    O_NOT   = ~I_X; 
assign    O_BUF   = I_X;
assign    O_TBUF  = (I_E) ? I_X : 1'bz;
assign    O_NAND  = ~(I_X & I_Y);
assign    O_NOR   = ~(I_X | I_Y);
assign    O_XOR   = I_X ^ I_Y;
assign    O_XNOR  = ~(I_X ^ I_Y);

endmodule


















