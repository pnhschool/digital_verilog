/*====================================

     MODULE : G711_DECODER_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module G711_DECODER_TOP(
	NRST          , 
	CLK           ,
	I_LAW_SEL     ,
	I_DATAIN      , 
	I_DATAIN_VAL  , 
	I_PG_REQ      , 
	I_LAST_SAMPLE , 
	O_DATAOUT     ,  
	O_DATAOUT_VAL ,
	O_LAST_PCM    ,
	O_DEC_REQ     	
);


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter    S_RDY       = 2'b00;
parameter    S_PROCESS   = 2'b01;
parameter    S_WAIT_REQ  = 2'b10;
parameter    S_VALID_STB = 2'b11;


//=====================================
//
//          I/O PORTS 
//
//=====================================
input        NRST          ; 
input        CLK           ;
input  	     I_LAW_SEL     ; 
input[7:0] 	 I_DATAIN      ; 
input  	     I_DATAIN_VAL  ; 
input  	     I_PG_REQ      ; 
input  	     I_LAST_SAMPLE ; 
output[15:0] O_DATAOUT     ;  
output 	     O_DATAOUT_VAL ; 
output 	     O_LAST_PCM    ;
output 	     O_DEC_REQ     ;


//=====================================
//
//          REGISTERS
//
//=====================================
reg[7:0]     r_inbuf       ; 
reg[13:0]    r_opcm        ; 
reg          r_en_1d       ; 
reg          r_en_2d       ; 
reg          r_last_pcm    ; 
reg[1:0]     r_pstate      ;


//=====================================
//
//          WIRES
//
//=====================================
reg[12:0]    w_alaw_rom    ;
reg[13:0]    w_ulaw_rom    ;

wire[7:0]    w_ineven_inv  ;

//=====================================
//
//          MAIN
//
//=====================================
// r_inbuf register
always @(negedge NRST or posedge CLK)
	if(~NRST) begin
		r_inbuf <= #1 8'h00;
	end
	else begin
		if(I_DATAIN_VAL)
			r_inbuf <= #1 I_DATAIN;
		else
			r_inbuf <= #1 r_inbuf;
	end

// w_ineven_inv
assign  w_ineven_inv = r_inbuf ^ 8'h55;

// w_alaw_rom
always @(w_ineven_inv)
	case(w_ineven_inv)
	8'h00   : w_alaw_rom  <= 13'h1D50; // -688 
	8'h01   : w_alaw_rom  <= 13'h1D70; // -656 
	8'h02   : w_alaw_rom  <= 13'h1D10; // -752 
	8'h03   : w_alaw_rom  <= 13'h1D30; // -720 
	8'h04   : w_alaw_rom  <= 13'h1DD0; // -560 
	8'h05   : w_alaw_rom  <= 13'h1DF0; // -528 
	8'h06   : w_alaw_rom  <= 13'h1D90; // -624 
	8'h07   : w_alaw_rom  <= 13'h1DB0; // -592 
	8'h08   : w_alaw_rom  <= 13'h1C50; // -944 
	8'h09   : w_alaw_rom  <= 13'h1C70; // -912 
	8'h0A   : w_alaw_rom  <= 13'h1C10; // -1008
	8'h0B   : w_alaw_rom  <= 13'h1C30; // -976 
	8'h0C   : w_alaw_rom  <= 13'h1CD0; // -816 
	8'h0D   : w_alaw_rom  <= 13'h1CF0; // -784 
	8'h0E   : w_alaw_rom  <= 13'h1C90; // -880 
	8'h0F   : w_alaw_rom  <= 13'h1CB0; // -848 
	8'h10   : w_alaw_rom  <= 13'h1EA8; // -344 
	8'h11   : w_alaw_rom  <= 13'h1EB8; // -328 
	8'h12   : w_alaw_rom  <= 13'h1E88; // -376 
	8'h13   : w_alaw_rom  <= 13'h1E98; // -360 
	8'h14   : w_alaw_rom  <= 13'h1EE8; // -280 
	8'h15   : w_alaw_rom  <= 13'h1EF8; // -264 
	8'h16   : w_alaw_rom  <= 13'h1EC8; // -312 
	8'h17   : w_alaw_rom  <= 13'h1ED8; // -296 
	8'h18   : w_alaw_rom  <= 13'h1E28; // -472 
	8'h19   : w_alaw_rom  <= 13'h1E38; // -456 
	8'h1A   : w_alaw_rom  <= 13'h1E08; // -504 
	8'h1B   : w_alaw_rom  <= 13'h1E18; // -488 
	8'h1C   : w_alaw_rom  <= 13'h1E68; // -408 
	8'h1D   : w_alaw_rom  <= 13'h1E78; // -392 
	8'h1E   : w_alaw_rom  <= 13'h1E48; // -440 
	8'h1F   : w_alaw_rom  <= 13'h1E58; // -424 
	8'h20   : w_alaw_rom  <= 13'h1540; // -2752
	8'h21   : w_alaw_rom  <= 13'h15C0; // -2624
	8'h22   : w_alaw_rom  <= 13'h1440; // -3008
	8'h23   : w_alaw_rom  <= 13'h14C0; // -2880
	8'h24   : w_alaw_rom  <= 13'h1740; // -2240
	8'h25   : w_alaw_rom  <= 13'h17C0; // -2112
	8'h26   : w_alaw_rom  <= 13'h1640; // -2496
	8'h27   : w_alaw_rom  <= 13'h16C0; // -2368
	8'h28   : w_alaw_rom  <= 13'h1140; // -3776
	8'h29   : w_alaw_rom  <= 13'h11C0; // -3648
	8'h2A   : w_alaw_rom  <= 13'h1040; // -4032
	8'h2B   : w_alaw_rom  <= 13'h10C0; // -3904
	8'h2C   : w_alaw_rom  <= 13'h1340; // -3264
	8'h2D   : w_alaw_rom  <= 13'h13C0; // -3136
	8'h2E   : w_alaw_rom  <= 13'h1240; // -3520
	8'h2F   : w_alaw_rom  <= 13'h12C0; // -3392
	8'h30   : w_alaw_rom  <= 13'h1AA0; // -1376
	8'h31   : w_alaw_rom  <= 13'h1AE0; // -1312
	8'h32   : w_alaw_rom  <= 13'h1A20; // -1504
	8'h33   : w_alaw_rom  <= 13'h1A60; // -1440
	8'h34   : w_alaw_rom  <= 13'h1BA0; // -1120
	8'h35   : w_alaw_rom  <= 13'h1BE0; // -1056
	8'h36   : w_alaw_rom  <= 13'h1B20; // -1248
	8'h37   : w_alaw_rom  <= 13'h1B60; // -1184
	8'h38   : w_alaw_rom  <= 13'h18A0; // -1888
	8'h39   : w_alaw_rom  <= 13'h18E0; // -1824
	8'h3A   : w_alaw_rom  <= 13'h1820; // -2016
	8'h3B   : w_alaw_rom  <= 13'h1860; // -1952
	8'h3C   : w_alaw_rom  <= 13'h19A0; // -1632
	8'h3D   : w_alaw_rom  <= 13'h19E0; // -1568
	8'h3E   : w_alaw_rom  <= 13'h1920; // -1760
	8'h3F   : w_alaw_rom  <= 13'h1960; // -1696
	8'h40   : w_alaw_rom  <= 13'h1FD5; // -043 
	8'h41   : w_alaw_rom  <= 13'h1FD7; // -041 
	8'h42   : w_alaw_rom  <= 13'h1FD1; // -047 
	8'h43   : w_alaw_rom  <= 13'h1FD3; // -045 
	8'h44   : w_alaw_rom  <= 13'h1FDD; // -035 
	8'h45   : w_alaw_rom  <= 13'h1FDF; // -033 
	8'h46   : w_alaw_rom  <= 13'h1FD9; // -039 
	8'h47   : w_alaw_rom  <= 13'h1FDB; // -037 
	8'h48   : w_alaw_rom  <= 13'h1FC5; // -059 
	8'h49   : w_alaw_rom  <= 13'h1FC7; // -057 
	8'h4A   : w_alaw_rom  <= 13'h1FC1; // -063 
	8'h4B   : w_alaw_rom  <= 13'h1FC3; // -061 
	8'h4C   : w_alaw_rom  <= 13'h1FCD; // -051 
	8'h4D   : w_alaw_rom  <= 13'h1FCF; // -049 
	8'h4E   : w_alaw_rom  <= 13'h1FC9; // -055 
	8'h4F   : w_alaw_rom  <= 13'h1FCB; // -053 
	8'h50   : w_alaw_rom  <= 13'h1FF5; // -011 
	8'h51   : w_alaw_rom  <= 13'h1FF7; // -009 
	8'h52   : w_alaw_rom  <= 13'h1FF1; // -015 
	8'h53   : w_alaw_rom  <= 13'h1FF3; // -013 
	8'h54   : w_alaw_rom  <= 13'h1FFD; // -003 
	8'h55   : w_alaw_rom  <= 13'h1FFF; // -001 
	8'h56   : w_alaw_rom  <= 13'h1FF9; // -007 
	8'h57   : w_alaw_rom  <= 13'h1FFB; // -005 
	8'h58   : w_alaw_rom  <= 13'h1FE5; // -027 
	8'h59   : w_alaw_rom  <= 13'h1FE7; // -025 
	8'h5A   : w_alaw_rom  <= 13'h1FE1; // -031 
	8'h5B   : w_alaw_rom  <= 13'h1FE3; // -029 
	8'h5C   : w_alaw_rom  <= 13'h1FED; // -019 
	8'h5D   : w_alaw_rom  <= 13'h1FEF; // -017 
	8'h5E   : w_alaw_rom  <= 13'h1FE9; // -023 
	8'h5F   : w_alaw_rom  <= 13'h1FEB; // -021 
	8'h60   : w_alaw_rom  <= 13'h1F54; // -172 
	8'h61   : w_alaw_rom  <= 13'h1F5C; // -164 
	8'h62   : w_alaw_rom  <= 13'h1F44; // -188 
	8'h63   : w_alaw_rom  <= 13'h1F4C; // -180 
	8'h64   : w_alaw_rom  <= 13'h1F74; // -140 
	8'h65   : w_alaw_rom  <= 13'h1F7C; // -132 
	8'h66   : w_alaw_rom  <= 13'h1F64; // -156 
	8'h67   : w_alaw_rom  <= 13'h1F6C; // -148 
	8'h68   : w_alaw_rom  <= 13'h1F14; // -236 
	8'h69   : w_alaw_rom  <= 13'h1F1C; // -228 
	8'h6A   : w_alaw_rom  <= 13'h1F04; // -252 
	8'h6B   : w_alaw_rom  <= 13'h1F0C; // -244 
	8'h6C   : w_alaw_rom  <= 13'h1F34; // -204 
	8'h6D   : w_alaw_rom  <= 13'h1F3C; // -196 
	8'h6E   : w_alaw_rom  <= 13'h1F24; // -220 
	8'h6F   : w_alaw_rom  <= 13'h1F2C; // -212 
	8'h70   : w_alaw_rom  <= 13'h1FAA; // -086 
	8'h71   : w_alaw_rom  <= 13'h1FAE; // -082 
	8'h72   : w_alaw_rom  <= 13'h1FA2; // -094 
	8'h73   : w_alaw_rom  <= 13'h1FA6; // -090 
	8'h74   : w_alaw_rom  <= 13'h1FBA; // -070 
	8'h75   : w_alaw_rom  <= 13'h1FBE; // -066 
	8'h76   : w_alaw_rom  <= 13'h1FB2; // -078 
	8'h77   : w_alaw_rom  <= 13'h1FB6; // -074 
	8'h78   : w_alaw_rom  <= 13'h1F8A; // -118 
	8'h79   : w_alaw_rom  <= 13'h1F8E; // -114 
	8'h7A   : w_alaw_rom  <= 13'h1F82; // -126 
	8'h7B   : w_alaw_rom  <= 13'h1F86; // -122 
	8'h7C   : w_alaw_rom  <= 13'h1F9A; // -102 
	8'h7D   : w_alaw_rom  <= 13'h1F9E; // -098 
	8'h7E   : w_alaw_rom  <= 13'h1F92; // -110 
	8'h7F   : w_alaw_rom  <= 13'h1F96; // -106 
	8'h80   : w_alaw_rom  <= 13'h02B0; // 0688 
	8'h81   : w_alaw_rom  <= 13'h0290; // 0656 
	8'h82   : w_alaw_rom  <= 13'h02F0; // 0752 
	8'h83   : w_alaw_rom  <= 13'h02D0; // 0720 
	8'h84   : w_alaw_rom  <= 13'h0230; // 0560 
	8'h85   : w_alaw_rom  <= 13'h0210; // 0528 
	8'h86   : w_alaw_rom  <= 13'h0270; // 0624 
	8'h87   : w_alaw_rom  <= 13'h0250; // 0592 
	8'h88   : w_alaw_rom  <= 13'h03B0; // 0944 
	8'h89   : w_alaw_rom  <= 13'h0390; // 0912 
	8'h8A   : w_alaw_rom  <= 13'h03F0; // 1008 
	8'h8B   : w_alaw_rom  <= 13'h03D0; // 0976 
	8'h8C   : w_alaw_rom  <= 13'h0330; // 0816 
	8'h8D   : w_alaw_rom  <= 13'h0310; // 0784 
	8'h8E   : w_alaw_rom  <= 13'h0370; // 0880 
	8'h8F   : w_alaw_rom  <= 13'h0350; // 0848 
	8'h90   : w_alaw_rom  <= 13'h0158; // 0344 
	8'h91   : w_alaw_rom  <= 13'h0148; // 0328 
	8'h92   : w_alaw_rom  <= 13'h0178; // 0376 
	8'h93   : w_alaw_rom  <= 13'h0168; // 0360 
	8'h94   : w_alaw_rom  <= 13'h0118; // 0280 
	8'h95   : w_alaw_rom  <= 13'h0108; // 0264 
	8'h96   : w_alaw_rom  <= 13'h0138; // 0312 
	8'h97   : w_alaw_rom  <= 13'h0128; // 0296 
	8'h98   : w_alaw_rom  <= 13'h01D8; // 0472 
	8'h99   : w_alaw_rom  <= 13'h01C8; // 0456 
	8'h9A   : w_alaw_rom  <= 13'h01F8; // 0504 
	8'h9B   : w_alaw_rom  <= 13'h01E8; // 0488 
	8'h9C   : w_alaw_rom  <= 13'h0198; // 0408 
	8'h9D   : w_alaw_rom  <= 13'h0188; // 0392 
	8'h9E   : w_alaw_rom  <= 13'h01B8; // 0440 
	8'h9F   : w_alaw_rom  <= 13'h01A8; // 0424 
	8'hA0   : w_alaw_rom  <= 13'h0AC0; // 2752 
	8'hA1   : w_alaw_rom  <= 13'h0A40; // 2624 
	8'hA2   : w_alaw_rom  <= 13'h0BC0; // 3008 
	8'hA3   : w_alaw_rom  <= 13'h0B40; // 2880 
	8'hA4   : w_alaw_rom  <= 13'h08C0; // 2240 
	8'hA5   : w_alaw_rom  <= 13'h0840; // 2112 
	8'hA6   : w_alaw_rom  <= 13'h09C0; // 2496 
	8'hA7   : w_alaw_rom  <= 13'h0940; // 2368 
	8'hA8   : w_alaw_rom  <= 13'h0EC0; // 3776 
	8'hA9   : w_alaw_rom  <= 13'h0E40; // 3648 
	8'hAA   : w_alaw_rom  <= 13'h0FC0; // 4032 
	8'hAB   : w_alaw_rom  <= 13'h0F40; // 3904 
	8'hAC   : w_alaw_rom  <= 13'h0CC0; // 3264 
	8'hAD   : w_alaw_rom  <= 13'h0C40; // 3136 
	8'hAE   : w_alaw_rom  <= 13'h0DC0; // 3520 
	8'hAF   : w_alaw_rom  <= 13'h0D40; // 3392 
	8'hB0   : w_alaw_rom  <= 13'h0560; // 1376 
	8'hB1   : w_alaw_rom  <= 13'h0520; // 1312 
	8'hB2   : w_alaw_rom  <= 13'h05E0; // 1504 
	8'hB3   : w_alaw_rom  <= 13'h05A0; // 1440 
	8'hB4   : w_alaw_rom  <= 13'h0460; // 1120 
	8'hB5   : w_alaw_rom  <= 13'h0420; // 1056 
	8'hB6   : w_alaw_rom  <= 13'h04E0; // 1248 
	8'hB7   : w_alaw_rom  <= 13'h04A0; // 1184 
	8'hB8   : w_alaw_rom  <= 13'h0760; // 1888 
	8'hB9   : w_alaw_rom  <= 13'h0720; // 1824 
	8'hBA   : w_alaw_rom  <= 13'h07E0; // 2016 
	8'hBB   : w_alaw_rom  <= 13'h07A0; // 1952 
	8'hBC   : w_alaw_rom  <= 13'h0660; // 1632 
	8'hBD   : w_alaw_rom  <= 13'h0620; // 1568 
	8'hBE   : w_alaw_rom  <= 13'h06E0; // 1760 
	8'hBF   : w_alaw_rom  <= 13'h06A0; // 1696 
	8'hC0   : w_alaw_rom  <= 13'h002B; // 0043 
	8'hC1   : w_alaw_rom  <= 13'h0029; // 0041 
	8'hC2   : w_alaw_rom  <= 13'h002F; // 0047 
	8'hC3   : w_alaw_rom  <= 13'h002D; // 0045 
	8'hC4   : w_alaw_rom  <= 13'h0023; // 0035 
	8'hC5   : w_alaw_rom  <= 13'h0021; // 0033 
	8'hC6   : w_alaw_rom  <= 13'h0027; // 0039 
	8'hC7   : w_alaw_rom  <= 13'h0025; // 0037 
	8'hC8   : w_alaw_rom  <= 13'h003B; // 0059 
	8'hC9   : w_alaw_rom  <= 13'h0039; // 0057 
	8'hCA   : w_alaw_rom  <= 13'h003F; // 0063 
	8'hCB   : w_alaw_rom  <= 13'h003D; // 0061 
	8'hCC   : w_alaw_rom  <= 13'h0033; // 0051 
	8'hCD   : w_alaw_rom  <= 13'h0031; // 0049 
	8'hCE   : w_alaw_rom  <= 13'h0037; // 0055 
	8'hCF   : w_alaw_rom  <= 13'h0035; // 0053 
	8'hD0   : w_alaw_rom  <= 13'h000B; // 0011 
	8'hD1   : w_alaw_rom  <= 13'h0009; // 0009 
	8'hD2   : w_alaw_rom  <= 13'h000F; // 0015 
	8'hD3   : w_alaw_rom  <= 13'h000D; // 0013 
	8'hD4   : w_alaw_rom  <= 13'h0003; // 0003 
	8'hD5   : w_alaw_rom  <= 13'h0001; // 0001 
	8'hD6   : w_alaw_rom  <= 13'h0007; // 0007 
	8'hD7   : w_alaw_rom  <= 13'h0005; // 0005 
	8'hD8   : w_alaw_rom  <= 13'h001B; // 0027 
	8'hD9   : w_alaw_rom  <= 13'h0019; // 0025 
	8'hDA   : w_alaw_rom  <= 13'h001F; // 0031 
	8'hDB   : w_alaw_rom  <= 13'h001D; // 0029 
	8'hDC   : w_alaw_rom  <= 13'h0013; // 0019 
	8'hDD   : w_alaw_rom  <= 13'h0011; // 0017 
	8'hDE   : w_alaw_rom  <= 13'h0017; // 0023 
	8'hDF   : w_alaw_rom  <= 13'h0015; // 0021 
	8'hE0   : w_alaw_rom  <= 13'h00AC; // 0172 
	8'hE1   : w_alaw_rom  <= 13'h00A4; // 0164 
	8'hE2   : w_alaw_rom  <= 13'h00BC; // 0188 
	8'hE3   : w_alaw_rom  <= 13'h00B4; // 0180 
	8'hE4   : w_alaw_rom  <= 13'h008C; // 0140 
	8'hE5   : w_alaw_rom  <= 13'h0084; // 0132 
	8'hE6   : w_alaw_rom  <= 13'h009C; // 0156 
	8'hE7   : w_alaw_rom  <= 13'h0094; // 0148 
	8'hE8   : w_alaw_rom  <= 13'h00EC; // 0236 
	8'hE9   : w_alaw_rom  <= 13'h00E4; // 0228 
	8'hEA   : w_alaw_rom  <= 13'h00FC; // 0252 
	8'hEB   : w_alaw_rom  <= 13'h00F4; // 0244 
	8'hEC   : w_alaw_rom  <= 13'h00CC; // 0204 
	8'hED   : w_alaw_rom  <= 13'h00C4; // 0196 
	8'hEE   : w_alaw_rom  <= 13'h00DC; // 0220 
	8'hEF   : w_alaw_rom  <= 13'h00D4; // 0212 
	8'hF0   : w_alaw_rom  <= 13'h0056; // 0086 
	8'hF1   : w_alaw_rom  <= 13'h0052; // 0082 
	8'hF2   : w_alaw_rom  <= 13'h005E; // 0094 
	8'hF3   : w_alaw_rom  <= 13'h005A; // 0090 
	8'hF4   : w_alaw_rom  <= 13'h0046; // 0070 
	8'hF5   : w_alaw_rom  <= 13'h0042; // 0066 
	8'hF6   : w_alaw_rom  <= 13'h004E; // 0078 
	8'hF7   : w_alaw_rom  <= 13'h004A; // 0074 
	8'hF8   : w_alaw_rom  <= 13'h0076; // 0118 
	8'hF9   : w_alaw_rom  <= 13'h0072; // 0114 
	8'hFA   : w_alaw_rom  <= 13'h007E; // 0126 
	8'hFB   : w_alaw_rom  <= 13'h007A; // 0122 
	8'hFC   : w_alaw_rom  <= 13'h0066; // 0102 
	8'hFD   : w_alaw_rom  <= 13'h0062; // 0098 
	8'hFE   : w_alaw_rom  <= 13'h006E; // 0110 
	8'hFF   : w_alaw_rom  <= 13'h006A; // 0106 
	default : w_alaw_rom  <= 13'h0000;
	endcase

// w_ulaw_rom
always @(r_inbuf)
	case(r_inbuf)
	8'h00   : w_ulaw_rom  <= 14'h20A1; // -8031
	8'h01   : w_ulaw_rom  <= 14'h21A1; // -7775
	8'h02   : w_ulaw_rom  <= 14'h22A1; // -7519
	8'h03   : w_ulaw_rom  <= 14'h23A1; // -7263
	8'h04   : w_ulaw_rom  <= 14'h24A1; // -7007
	8'h05   : w_ulaw_rom  <= 14'h25A1; // -6751
	8'h06   : w_ulaw_rom  <= 14'h26A1; // -6495
	8'h07   : w_ulaw_rom  <= 14'h27A1; // -6239
	8'h08   : w_ulaw_rom  <= 14'h28A1; // -5983
	8'h09   : w_ulaw_rom  <= 14'h29A1; // -5727
	8'h0A   : w_ulaw_rom  <= 14'h2AA1; // -5471
	8'h0B   : w_ulaw_rom  <= 14'h2BA1; // -5215
	8'h0C   : w_ulaw_rom  <= 14'h2CA1; // -4959
	8'h0D   : w_ulaw_rom  <= 14'h2DA1; // -4703
	8'h0E   : w_ulaw_rom  <= 14'h2EA1; // -4447
	8'h0F   : w_ulaw_rom  <= 14'h2FA1; // -4191
	8'h10   : w_ulaw_rom  <= 14'h3061; // -3999
	8'h11   : w_ulaw_rom  <= 14'h30E1; // -3871
	8'h12   : w_ulaw_rom  <= 14'h3161; // -3743
	8'h13   : w_ulaw_rom  <= 14'h31E1; // -3615
	8'h14   : w_ulaw_rom  <= 14'h3261; // -3487
	8'h15   : w_ulaw_rom  <= 14'h32E1; // -3359
	8'h16   : w_ulaw_rom  <= 14'h3361; // -3231
	8'h17   : w_ulaw_rom  <= 14'h33E1; // -3103
	8'h18   : w_ulaw_rom  <= 14'h3461; // -2975
	8'h19   : w_ulaw_rom  <= 14'h34E1; // -2847
	8'h1A   : w_ulaw_rom  <= 14'h3561; // -2719
	8'h1B   : w_ulaw_rom  <= 14'h35E1; // -2591
	8'h1C   : w_ulaw_rom  <= 14'h3661; // -2463
	8'h1D   : w_ulaw_rom  <= 14'h36E1; // -2335
	8'h1E   : w_ulaw_rom  <= 14'h3761; // -2207
	8'h1F   : w_ulaw_rom  <= 14'h37E1; // -2079
	8'h20   : w_ulaw_rom  <= 14'h3841; // -1983
	8'h21   : w_ulaw_rom  <= 14'h3881; // -1919
	8'h22   : w_ulaw_rom  <= 14'h38C1; // -1855
	8'h23   : w_ulaw_rom  <= 14'h3901; // -1791
	8'h24   : w_ulaw_rom  <= 14'h3941; // -1727
	8'h25   : w_ulaw_rom  <= 14'h3981; // -1663
	8'h26   : w_ulaw_rom  <= 14'h39C1; // -1599
	8'h27   : w_ulaw_rom  <= 14'h3A01; // -1535
	8'h28   : w_ulaw_rom  <= 14'h3A41; // -1471
	8'h29   : w_ulaw_rom  <= 14'h3A81; // -1407
	8'h2A   : w_ulaw_rom  <= 14'h3AC1; // -1343
	8'h2B   : w_ulaw_rom  <= 14'h3B01; // -1279
	8'h2C   : w_ulaw_rom  <= 14'h3B41; // -1215
	8'h2D   : w_ulaw_rom  <= 14'h3B81; // -1151
	8'h2E   : w_ulaw_rom  <= 14'h3BC1; // -1087
	8'h2F   : w_ulaw_rom  <= 14'h3C01; // -1023
	8'h30   : w_ulaw_rom  <= 14'h3C31; // -975
	8'h31   : w_ulaw_rom  <= 14'h3C51; // -943
	8'h32   : w_ulaw_rom  <= 14'h3C71; // -911
	8'h33   : w_ulaw_rom  <= 14'h3C91; // -879
	8'h34   : w_ulaw_rom  <= 14'h3CB1; // -847
	8'h35   : w_ulaw_rom  <= 14'h3CD1; // -815
	8'h36   : w_ulaw_rom  <= 14'h3CF1; // -783
	8'h37   : w_ulaw_rom  <= 14'h3D11; // -751
	8'h38   : w_ulaw_rom  <= 14'h3D31; // -719
	8'h39   : w_ulaw_rom  <= 14'h3D51; // -687
	8'h3A   : w_ulaw_rom  <= 14'h3D71; // -655
	8'h3B   : w_ulaw_rom  <= 14'h3D91; // -623
	8'h3C   : w_ulaw_rom  <= 14'h3DB1; // -591
	8'h3D   : w_ulaw_rom  <= 14'h3DD1; // -559
	8'h3E   : w_ulaw_rom  <= 14'h3DF1; // -527
	8'h3F   : w_ulaw_rom  <= 14'h3E11; // -495
	8'h40   : w_ulaw_rom  <= 14'h3E29; // -471
	8'h41   : w_ulaw_rom  <= 14'h3E39; // -455
	8'h42   : w_ulaw_rom  <= 14'h3E49; // -439
	8'h43   : w_ulaw_rom  <= 14'h3E59; // -423
	8'h44   : w_ulaw_rom  <= 14'h3E69; // -407
	8'h45   : w_ulaw_rom  <= 14'h3E79; // -391
	8'h46   : w_ulaw_rom  <= 14'h3E89; // -375
	8'h47   : w_ulaw_rom  <= 14'h3E99; // -359
	8'h48   : w_ulaw_rom  <= 14'h3EA9; // -343
	8'h49   : w_ulaw_rom  <= 14'h3EB9; // -327
	8'h4A   : w_ulaw_rom  <= 14'h3EC9; // -311
	8'h4B   : w_ulaw_rom  <= 14'h3ED9; // -295
	8'h4C   : w_ulaw_rom  <= 14'h3EE9; // -279
	8'h4D   : w_ulaw_rom  <= 14'h3EF9; // -263
	8'h4E   : w_ulaw_rom  <= 14'h3F09; // -247
	8'h4F   : w_ulaw_rom  <= 14'h3F19; // -231
	8'h50   : w_ulaw_rom  <= 14'h3F25; // -219
	8'h51   : w_ulaw_rom  <= 14'h3F2D; // -211
	8'h52   : w_ulaw_rom  <= 14'h3F35; // -203
	8'h53   : w_ulaw_rom  <= 14'h3F3D; // -195
	8'h54   : w_ulaw_rom  <= 14'h3F45; // -187
	8'h55   : w_ulaw_rom  <= 14'h3F4D; // -179
	8'h56   : w_ulaw_rom  <= 14'h3F55; // -171
	8'h57   : w_ulaw_rom  <= 14'h3F5D; // -163
	8'h58   : w_ulaw_rom  <= 14'h3F65; // -155
	8'h59   : w_ulaw_rom  <= 14'h3F6D; // -147
	8'h5A   : w_ulaw_rom  <= 14'h3F75; // -139
	8'h5B   : w_ulaw_rom  <= 14'h3F7D; // -131
	8'h5C   : w_ulaw_rom  <= 14'h3F85; // -123
	8'h5D   : w_ulaw_rom  <= 14'h3F8D; // -115
	8'h5E   : w_ulaw_rom  <= 14'h3F95; // -107
	8'h5F   : w_ulaw_rom  <= 14'h3F9D; // -099
	8'h60   : w_ulaw_rom  <= 14'h3FA3; // -093
	8'h61   : w_ulaw_rom  <= 14'h3FA7; // -089
	8'h62   : w_ulaw_rom  <= 14'h3FAB; // -085
	8'h63   : w_ulaw_rom  <= 14'h3FAF; // -081
	8'h64   : w_ulaw_rom  <= 14'h3FB3; // -077
	8'h65   : w_ulaw_rom  <= 14'h3FB7; // -073
	8'h66   : w_ulaw_rom  <= 14'h3FBB; // -069
	8'h67   : w_ulaw_rom  <= 14'h3FBF; // -065
	8'h68   : w_ulaw_rom  <= 14'h3FC3; // -061
	8'h69   : w_ulaw_rom  <= 14'h3FC7; // -057
	8'h6A   : w_ulaw_rom  <= 14'h3FCB; // -053
	8'h6B   : w_ulaw_rom  <= 14'h3FCF; // -049
	8'h6C   : w_ulaw_rom  <= 14'h3FD3; // -045
	8'h6D   : w_ulaw_rom  <= 14'h3FD7; // -041
	8'h6E   : w_ulaw_rom  <= 14'h3FDB; // -037
	8'h6F   : w_ulaw_rom  <= 14'h3FDF; // -033
	8'h70   : w_ulaw_rom  <= 14'h3FE2; // -030
	8'h71   : w_ulaw_rom  <= 14'h3FE4; // -028
	8'h72   : w_ulaw_rom  <= 14'h3FE6; // -026
	8'h73   : w_ulaw_rom  <= 14'h3FE8; // -024
	8'h74   : w_ulaw_rom  <= 14'h3FEA; // -022
	8'h75   : w_ulaw_rom  <= 14'h3FEC; // -020
	8'h76   : w_ulaw_rom  <= 14'h3FEE; // -018
	8'h77   : w_ulaw_rom  <= 14'h3FF0; // -016
	8'h78   : w_ulaw_rom  <= 14'h3FF2; // -014
	8'h79   : w_ulaw_rom  <= 14'h3FF4; // -012
	8'h7A   : w_ulaw_rom  <= 14'h3FF6; // -010
	8'h7B   : w_ulaw_rom  <= 14'h3FF8; // -008
	8'h7C   : w_ulaw_rom  <= 14'h3FFA; // -006
	8'h7D   : w_ulaw_rom  <= 14'h3FFC; // -004
	8'h7E   : w_ulaw_rom  <= 14'h3FFE; // -002
	8'h7F   : w_ulaw_rom  <= 14'h0000; // 0000
	8'h80   : w_ulaw_rom  <= 14'h1F5F; // 8031
	8'h81   : w_ulaw_rom  <= 14'h1E5F; // 7775
	8'h82   : w_ulaw_rom  <= 14'h1D5F; // 7519
	8'h83   : w_ulaw_rom  <= 14'h1C5F; // 7263
	8'h84   : w_ulaw_rom  <= 14'h1B5F; // 7007
	8'h85   : w_ulaw_rom  <= 14'h1A5F; // 6751
	8'h86   : w_ulaw_rom  <= 14'h195F; // 6495
	8'h87   : w_ulaw_rom  <= 14'h185F; // 6239
	8'h88   : w_ulaw_rom  <= 14'h175F; // 5983
	8'h89   : w_ulaw_rom  <= 14'h165F; // 5727
	8'h8A   : w_ulaw_rom  <= 14'h155F; // 5471
	8'h8B   : w_ulaw_rom  <= 14'h145F; // 5215
	8'h8C   : w_ulaw_rom  <= 14'h135F; // 4959
	8'h8D   : w_ulaw_rom  <= 14'h125F; // 4703
	8'h8E   : w_ulaw_rom  <= 14'h115F; // 4447
	8'h8F   : w_ulaw_rom  <= 14'h105F; // 4191
	8'h90   : w_ulaw_rom  <= 14'h0F9F; // 3999
	8'h91   : w_ulaw_rom  <= 14'h0F1F; // 3871
	8'h92   : w_ulaw_rom  <= 14'h0E9F; // 3743
	8'h93   : w_ulaw_rom  <= 14'h0E1F; // 3615
	8'h94   : w_ulaw_rom  <= 14'h0D9F; // 3487
	8'h95   : w_ulaw_rom  <= 14'h0D1F; // 3359
	8'h96   : w_ulaw_rom  <= 14'h0C9F; // 3231
	8'h97   : w_ulaw_rom  <= 14'h0C1F; // 3103
	8'h98   : w_ulaw_rom  <= 14'h0B9F; // 2975
	8'h99   : w_ulaw_rom  <= 14'h0B1F; // 2847
	8'h9A   : w_ulaw_rom  <= 14'h0A9F; // 2719
	8'h9B   : w_ulaw_rom  <= 14'h0A1F; // 2591
	8'h9C   : w_ulaw_rom  <= 14'h099F; // 2463
	8'h9D   : w_ulaw_rom  <= 14'h091F; // 2335
	8'h9E   : w_ulaw_rom  <= 14'h089F; // 2207
	8'h9F   : w_ulaw_rom  <= 14'h081F; // 2079
	8'hA0   : w_ulaw_rom  <= 14'h07BF; // 1983
	8'hA1   : w_ulaw_rom  <= 14'h077F; // 1919
	8'hA2   : w_ulaw_rom  <= 14'h073F; // 1855
	8'hA3   : w_ulaw_rom  <= 14'h06FF; // 1791
	8'hA4   : w_ulaw_rom  <= 14'h06BF; // 1727
	8'hA5   : w_ulaw_rom  <= 14'h067F; // 1663
	8'hA6   : w_ulaw_rom  <= 14'h063F; // 1599
	8'hA7   : w_ulaw_rom  <= 14'h05FF; // 1535
	8'hA8   : w_ulaw_rom  <= 14'h05BF; // 1471
	8'hA9   : w_ulaw_rom  <= 14'h057F; // 1407
	8'hAA   : w_ulaw_rom  <= 14'h053F; // 1343
	8'hAB   : w_ulaw_rom  <= 14'h04FF; // 1279
	8'hAC   : w_ulaw_rom  <= 14'h04BF; // 1215
	8'hAD   : w_ulaw_rom  <= 14'h047F; // 1151
	8'hAE   : w_ulaw_rom  <= 14'h043F; // 1087
	8'hAF   : w_ulaw_rom  <= 14'h03FF; // 1023
	8'hB0   : w_ulaw_rom  <= 14'h03CF; // 0975
	8'hB1   : w_ulaw_rom  <= 14'h03AF; // 0943
	8'hB2   : w_ulaw_rom  <= 14'h038F; // 0911
	8'hB3   : w_ulaw_rom  <= 14'h036F; // 0879
	8'hB4   : w_ulaw_rom  <= 14'h034F; // 0847
	8'hB5   : w_ulaw_rom  <= 14'h032F; // 0815
	8'hB6   : w_ulaw_rom  <= 14'h030F; // 0783
	8'hB7   : w_ulaw_rom  <= 14'h02EF; // 0751
	8'hB8   : w_ulaw_rom  <= 14'h02CF; // 0719
	8'hB9   : w_ulaw_rom  <= 14'h02AF; // 0687
	8'hBA   : w_ulaw_rom  <= 14'h028F; // 0655
	8'hBB   : w_ulaw_rom  <= 14'h026F; // 0623
	8'hBC   : w_ulaw_rom  <= 14'h024F; // 0591
	8'hBD   : w_ulaw_rom  <= 14'h022F; // 0559
	8'hBE   : w_ulaw_rom  <= 14'h020F; // 0527
	8'hBF   : w_ulaw_rom  <= 14'h01EF; // 0495
	8'hC0   : w_ulaw_rom  <= 14'h01D7; // 0471
	8'hC1   : w_ulaw_rom  <= 14'h01C7; // 0455
	8'hC2   : w_ulaw_rom  <= 14'h01B7; // 0439
	8'hC3   : w_ulaw_rom  <= 14'h01A7; // 0423
	8'hC4   : w_ulaw_rom  <= 14'h0197; // 0407
	8'hC5   : w_ulaw_rom  <= 14'h0187; // 0391
	8'hC6   : w_ulaw_rom  <= 14'h0177; // 0375
	8'hC7   : w_ulaw_rom  <= 14'h0167; // 0359
	8'hC8   : w_ulaw_rom  <= 14'h0157; // 0343
	8'hC9   : w_ulaw_rom  <= 14'h0147; // 0327
	8'hCA   : w_ulaw_rom  <= 14'h0137; // 0311
	8'hCB   : w_ulaw_rom  <= 14'h0127; // 0295
	8'hCC   : w_ulaw_rom  <= 14'h0117; // 0279
	8'hCD   : w_ulaw_rom  <= 14'h0107; // 0263
	8'hCE   : w_ulaw_rom  <= 14'h00F7; // 0247
	8'hCF   : w_ulaw_rom  <= 14'h00E7; // 0231
	8'hD0   : w_ulaw_rom  <= 14'h00DB; // 0219
	8'hD1   : w_ulaw_rom  <= 14'h00D3; // 0211
	8'hD2   : w_ulaw_rom  <= 14'h00CB; // 0203
	8'hD3   : w_ulaw_rom  <= 14'h00C3; // 0195
	8'hD4   : w_ulaw_rom  <= 14'h00BB; // 0187
	8'hD5   : w_ulaw_rom  <= 14'h00B3; // 0179
	8'hD6   : w_ulaw_rom  <= 14'h00AB; // 0171
	8'hD7   : w_ulaw_rom  <= 14'h00A3; // 0163
	8'hD8   : w_ulaw_rom  <= 14'h009B; // 0155
	8'hD9   : w_ulaw_rom  <= 14'h0093; // 0147
	8'hDA   : w_ulaw_rom  <= 14'h008B; // 0139
	8'hDB   : w_ulaw_rom  <= 14'h0083; // 0131
	8'hDC   : w_ulaw_rom  <= 14'h007B; // 0123
	8'hDD   : w_ulaw_rom  <= 14'h0073; // 0115
	8'hDE   : w_ulaw_rom  <= 14'h006B; // 0107
	8'hDF   : w_ulaw_rom  <= 14'h0063; // 0099
	8'hE0   : w_ulaw_rom  <= 14'h005D; // 0093
	8'hE1   : w_ulaw_rom  <= 14'h0059; // 0089
	8'hE2   : w_ulaw_rom  <= 14'h0055; // 0085
	8'hE3   : w_ulaw_rom  <= 14'h0051; // 0081
	8'hE4   : w_ulaw_rom  <= 14'h004D; // 0077
	8'hE5   : w_ulaw_rom  <= 14'h0049; // 0073
	8'hE6   : w_ulaw_rom  <= 14'h0045; // 0069
	8'hE7   : w_ulaw_rom  <= 14'h0041; // 0065
	8'hE8   : w_ulaw_rom  <= 14'h003D; // 0061
	8'hE9   : w_ulaw_rom  <= 14'h0039; // 0057
	8'hEA   : w_ulaw_rom  <= 14'h0035; // 0053
	8'hEB   : w_ulaw_rom  <= 14'h0031; // 0049
	8'hEC   : w_ulaw_rom  <= 14'h002D; // 0045
	8'hED   : w_ulaw_rom  <= 14'h0029; // 0041
	8'hEE   : w_ulaw_rom  <= 14'h0025; // 0037
	8'hEF   : w_ulaw_rom  <= 14'h0021; // 0033
	8'hF0   : w_ulaw_rom  <= 14'h001E; // 0030
	8'hF1   : w_ulaw_rom  <= 14'h001C; // 0028
	8'hF2   : w_ulaw_rom  <= 14'h001A; // 0026
	8'hF3   : w_ulaw_rom  <= 14'h0018; // 0024
	8'hF4   : w_ulaw_rom  <= 14'h0016; // 0022
	8'hF5   : w_ulaw_rom  <= 14'h0014; // 0020
	8'hF6   : w_ulaw_rom  <= 14'h0012; // 0018
	8'hF7   : w_ulaw_rom  <= 14'h0010; // 0016
	8'hF8   : w_ulaw_rom  <= 14'h000E; // 0014
	8'hF9   : w_ulaw_rom  <= 14'h000C; // 0012
	8'hFA   : w_ulaw_rom  <= 14'h000A; // 0010
	8'hFB   : w_ulaw_rom  <= 14'h0008; // 0008
	8'hFC   : w_ulaw_rom  <= 14'h0006; // 0006
	8'hFD   : w_ulaw_rom  <= 14'h0004; // 0004
	8'hFE   : w_ulaw_rom  <= 14'h0002; // 0002
	8'hFF   : w_ulaw_rom  <= 14'h0000; // 0000
	default : w_ulaw_rom  <= 14'h0000;
	endcase

// r_opcm register
always @(negedge NRST or posedge CLK)
	if(~NRST) begin
		r_opcm         <= #1 14'h0000;
	end
	else begin
		if(r_en_1d)
			if(~I_LAW_SEL)
				r_opcm <= #1 {w_alaw_rom,1'b0}; 
			else
				r_opcm <= #1 w_ulaw_rom;
		else
			r_opcm     <= # 1 r_opcm;
	end

// r_en_1d, r_en_2d register
always @(negedge NRST or posedge CLK)
	if(~NRST) begin
		r_en_1d <= #1 1'b0;
		r_en_2d <= #1 1'b0;
	end
	else begin
		r_en_1d <= #1 I_DATAIN_VAL;
		r_en_2d <= #1 r_en_1d;
	end

//// FSM ////
// r_pstate register
always @(negedge NRST or posedge CLK)
	if(~NRST) begin
		r_pstate         <= #1 S_RDY;
	end
	else begin
		case(r_pstate)
		S_RDY : 
			if(I_DATAIN_VAL)
				r_pstate <= #1 S_PROCESS;
			else
				r_pstate <= #1 r_pstate;
		S_PROCESS :
			if(r_en_2d)
				r_pstate <= #1 S_WAIT_REQ;
			else
				r_pstate <= #1 r_pstate;
		S_WAIT_REQ :
			if(I_PG_REQ)
				r_pstate <= #1 S_VALID_STB;
			else
				r_pstate <= #1 r_pstate; 
		S_VALID_STB : 
			r_pstate     <= #1 S_RDY;
		default	:
			r_pstate     <= #1 r_pstate;
		endcase
	end

// r_last_pcm register
always @(negedge NRST or posedge CLK)
	if(~NRST) begin
		r_last_pcm         <= #1 1'b0;
	end	
	else begin
		if((I_DATAIN_VAL & I_LAST_SAMPLE) == 1'b0)
			if((r_pstate == S_VALID_STB) == 1'b0)
				r_last_pcm <= #1 r_last_pcm;
			else
				r_last_pcm <= #1 1'b0; 
		else
			r_last_pcm     <= #1 1'b1;
	end


//=====================================
//
//          OUTPUT ASSIGN
//
//=====================================
assign  O_DATAOUT     = {r_opcm,2'b00};
assign  O_DEC_REQ     = (r_pstate == S_RDY);
assign  O_DATAOUT_VAL = (r_pstate == S_VALID_STB); 
assign  O_LAST_PCM    = (r_pstate == S_VALID_STB) & r_last_pcm;

endmodule

