/*====================================

     MODULE : tb_G711_DECODER_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_G711_DECODER_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================
parameter HP_CLK = 10; // Half period of Clock


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg          NRST          ; 
reg          CLK           ;
reg  	     I_LAW_SEL     ; 
reg[7:0] 	 I_DATAIN      ; 
reg  	     I_DATAIN_VAL  ; 
reg  	     I_PG_REQ      ; 
reg  	     I_LAST_SAMPLE ; 
wire[15:0]   O_DATAOUT     ;  
wire 	     O_DATAOUT_VAL ; 
wire 	     O_LAST_PCM    ;
wire 	     O_DEC_REQ     ;


//=====================================
//
//      SUPPLEMENTARY SIGNALS
//
//=====================================
// a-law LUT memory
reg[12:0]    alaw_lut_mem[0:255];
// u-law LUT memory
reg[13:0]    ulaw_lut_mem[0:255];


//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
G711_DECODER_TOP uut0_G711_DECODER_TOP(
	.NRST         (NRST         ),
	.CLK          (CLK          ),
 	.I_LAW_SEL    (I_LAW_SEL    ),
	.I_DATAIN     (I_DATAIN     ), 
	.I_DATAIN_VAL (I_DATAIN_VAL ), 
	.I_PG_REQ     (I_PG_REQ     ), 
	.I_LAST_SAMPLE(I_LAST_SAMPLE), 
	.O_DATAOUT    (O_DATAOUT    ),  
	.O_DATAOUT_VAL(O_DATAOUT_VAL),
	.O_LAST_PCM   (O_LAST_PCM   ),
	.O_DEC_REQ    (O_DEC_REQ    )	
);


//=====================================
//
//          STIMULI
//
//=====================================
// clock generation
initial begin
	CLK = 1'b0;
	forever CLK = #(HP_CLK) ~CLK;
end

// reset generation
initial begin
	NRST = 1'b1;
	@(posedge CLK)
	@(negedge CLK)
	NRST = 1'b0;
	repeat(2) @(negedge CLK);
	NRST = 1'b1;
end

// input signal generation
integer i;
initial begin
	$display("===== SIM START =====");
	// insert your code ...

	// read a-law LUT
	$readmemh("./testbench/alaw_lut.dat", alaw_lut_mem);	
	
	// read u-law LUT
	$readmemh("./testbench/ulaw_lut.dat", ulaw_lut_mem);

	// inital input value
	I_DATAIN      = 8'hx;
	I_DATAIN_VAL  = 1'b0;
	I_LAST_SAMPLE = 1'b0;
	I_LAW_SEL     = 1'b0;
	I_PG_REQ      = 1'b0;

	// reset time 
	@(negedge NRST);
	@(posedge NRST);
	@(negedge CLK);

	// a-law test
	I_LAW_SEL = 1'b0;
	for(i=0;i<256;i=i+1) begin
		wait(O_DEC_REQ == 1'b1) 
		@(posedge CLK);
		@(negedge CLK);
		I_DATAIN          = i;
		I_DATAIN_VAL      = 1'b1;
		if(i == 255)
			I_LAST_SAMPLE = 1'b1;
		else
			I_LAST_SAMPLE = 1'b0;
		I_PG_REQ          = 1'b0;

		@(negedge CLK);
		I_DATAIN          = 8'hx;
		I_DATAIN_VAL      = 1'b0;
		I_LAST_SAMPLE     = 1'b0;		
	
		repeat(1) @(negedge CLK);
		I_PG_REQ          = 1'b1;	
		
		wait(O_DATAOUT_VAL)
		@(posedge CLK);
		@(negedge CLK);
		I_PG_REQ          = 1'b0;
	end

	// 10 cycle delay
	repeat(10) @(posedge CLK);

	// u-law test
	I_LAW_SEL = 1'b1;
	for(i=0;i<256;i=i+1) begin
		wait(O_DEC_REQ == 1'b1) 
		@(posedge CLK);
		@(negedge CLK);
		I_DATAIN          = i;
		I_DATAIN_VAL      = 1'b1;
		if(i == 255)
			I_LAST_SAMPLE = 1'b1;
		else
			I_LAST_SAMPLE = 1'b0;
		I_PG_REQ          = 1'b0;

		@(negedge CLK);
		I_DATAIN          = 8'hx;
		I_DATAIN_VAL      = 1'b0;
		I_LAST_SAMPLE     = 1'b0;		
	
		repeat(3) @(negedge CLK);
		I_PG_REQ          = 1'b1;	
		
		wait(O_DATAOUT_VAL)
		@(posedge CLK);
		@(negedge CLK);
		I_PG_REQ          = 1'b0;
	end

	// dump finish
	repeat(10) @(negedge CLK);
	NRST = 1'b0;
					
	$display("=====  SIM END  =====");
	$finish;
end

// monitoring
integer   j; 
reg[7:0] datain_tmp;
//always @(posedge CLK) begin
initial begin
	// example
	//$display("[monitor] i_cnt_en=%u,o_cnt=%u @%6dns", i_cnt_en, o_cnt, $time); 

	// reset time 
	@(negedge NRST);
	@(posedge NRST);
	@(negedge CLK);

	// a-law output comapare
	for(j=0;j<256;j=j+1) begin
		wait(I_DATAIN_VAL)
		@(posedge CLK);
		datain_tmp = I_DATAIN;			

		wait(O_DATAOUT_VAL) 
		@(posedge CLK);
		$display("\
[monitor] (a-law) I_DATAIN=%3d => O_DATAOUT=%x,\n\
                                  Expected value=%x, %s\n", 
		datain_tmp,
		O_DATAOUT,
		{alaw_lut_mem[datain_tmp^8'h55],3'b0},
		(O_DATAOUT == {alaw_lut_mem[datain_tmp^8'h55],3'b0}) ? "(Correct)" : "(Wrong)");
	end	

	@(posedge I_LAW_SEL);
	$display("\n\n\n");
	// u-law output compare
	for(j=0;j<256;j=j+1) begin
		wait(I_DATAIN_VAL)
		@(posedge CLK);
		datain_tmp = I_DATAIN;			

		wait(O_DATAOUT_VAL) 
		@(posedge CLK);
		$display("\
[monitor] (u-law) I_DATAIN=%3d => O_DATAOUT=%x, \n\
                                  Expected value=%x, %s\n", 
		datain_tmp,
		O_DATAOUT,
		{ulaw_lut_mem[datain_tmp],2'b0},
		(O_DATAOUT == {ulaw_lut_mem[datain_tmp],2'b0}) ? "(Correct)" : "(Wrong)");
	end	
end

endmodule


