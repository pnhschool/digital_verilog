/*====================================

     MODULE : tb_ENC8TO3_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_ENC8TO3_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg[0:7]    I_DAT    ;
wire[2:0]   O_CODE   ; 

//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
ENC8TO3_TOP uut0_ENC8TO3_TOP(
	.I_DAT   (I_DAT )  ,
	.O_CODE  (O_CODE)     
);


//=====================================
//
//          STIMULUS
//
//=====================================
// input signal generation
reg[8:0] i;
initial begin
	$display("===== SIM START =====");
	// insert your code ...
	for(i = 9'b0_0000_0000; i <= 9'b0_1111_1111; i = i + 9'b0_0000_0001) begin
		I_DAT = i[7:0]; 
		#50;
 
		case(I_DAT)
		8'b1000_0000 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b000) ? "Correct" : "Wrong", $time);
		8'b0100_0000 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b001) ? "Correct" : "Wrong", $time);
		8'b0010_0000 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b010) ? "Correct" : "Wrong", $time);
		8'b0001_0000 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b011) ? "Correct" : "Wrong", $time);
		8'b0000_1000 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b100) ? "Correct" : "Wrong", $time);
		8'b0000_0100 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b101) ? "Correct" : "Wrong", $time);
		8'b0000_0010 : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b110) ? "Correct" : "Wrong", $time);
		8'b0000_0001 :  
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE, 
			(O_CODE === 3'b111) ? "Correct" : "Wrong", $time);
		default : 
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'bxxx) ? "Correct" : "Wrong", $time);
		endcase

		#50;
	end
	
	$display("=====  SIM END  =====");
	$finish;
end

endmodule


