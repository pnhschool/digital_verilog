/*====================================

     MODULE : tb_PRI_ENC8TO3_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_PRI_ENC8TO3_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg[0:7]    I_DAT  ;
wire[2:0]   O_CODE ;


//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
PRI_ENC8TO3_TOP uut0_PRI_ENC8TO3_TOP(
	.I_DAT  (I_DAT )  ,
	.O_CODE (O_CODE)     
);


//=====================================
//
//          STIMULUS
//
//=====================================
// input signal generation
reg[8:0] i;
initial begin
	$display("===== SIM START =====");
	// insert your code ...
	for(i = 9'b0_0000_0000; i <= 9'b0_1111_1111; i = i + 9'b0_0000_0001) begin
		I_DAT = i[7:0];
		#50;

		if(I_DAT[0] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b000) ? "Correct" : "Wrong", $time);
		else if(I_DAT[1] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b001) ? "Correct" : "Wrong", $time);
		else if(I_DAT[2] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b010) ? "Correct" : "Wrong", $time);
		else if(I_DAT[3] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b011) ? "Correct" : "Wrong", $time);
		else if(I_DAT[4] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b100) ? "Correct" : "Wrong", $time);
		else if(I_DAT[5] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b101) ? "Correct" : "Wrong", $time);
		else if(I_DAT[6] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b110) ? "Correct" : "Wrong", $time);
		else if(I_DAT[7] == 1'b1)
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'b111) ? "Correct" : "Wrong", $time);
		else
			$display("I_DAT = %8b ==> O_CODE = %3b (%s) @%0d ns", I_DAT, O_CODE,
			(O_CODE === 3'bxxx) ? "Correct" : "Wrong", $time);

		#50;
	end
	$display("=====  SIM END  =====");
	$finish;
end

endmodule


