/*====================================

     MODULE : tb_DEC3TO8_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_DEC3TO8_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg[2:0]   i_code ;
wire[0:7]  o_dat  ; 


//=====================================
//
//         VARIABLES, NETS AND SIGNALS 
//
//=====================================


//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
DEC3TO8_TOP uut0_DEC3TO8_TOP(
	.i_code  (i_code)  ,
	.o_dat   (o_dat )    
);


//=====================================
//
//          STIMULUS
//
//=====================================
// input generation
reg[3:0] i;
initial begin
	$display("===== SIM START =====");
	// insert your code ...
	for(i = 4'b0000; i <= 4'b1000; i = i + 4'b0001) begin
		i_code = i[2:0];
		#50;

		// check
		if(i_code == 3'b000)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b1000_0000) ? "Correct" : "Wrong", $time);
		else if(i_code == 3'b001)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0100_0000) ? "Correct" : "Wrong", $time);
		else if(i_code == 3'b010)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0010_0000) ? "Correct" : "Wrong", $time);
		else if(i_code == 3'b011)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0001_0000) ? "Correct" : "Wrong", $time);
		else if(i_code == 3'b100)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0000_1000) ? "Correct" : "Wrong", $time);
		else if(i_code == 3'b101)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0000_0100) ? "Correct" : "Wrong", $time);
		else if(i_code == 3'b110)
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0000_0010) ? "Correct" : "Wrong", $time);
		else 
			$display("i_code = %3b ==> o_dat = %8b (%s) @%0d ns", i_code, o_dat,
			          (o_dat == 8'b0000_0001) ? "Correct" : "Wrong", $time);

		#50;
	end
	$display("=====  SIM END  =====");
	$finish;
end

// vcd dump
initial begin
	$dumpfile("dump/sim_tb_DEC3TO8_TOP.vcd");
	$dumpvars(0, tb_DEC3TO8_TOP);
end

endmodule


