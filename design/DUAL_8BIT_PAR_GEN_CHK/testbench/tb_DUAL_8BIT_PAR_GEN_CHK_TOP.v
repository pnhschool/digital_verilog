/*====================================

     MODULE : tb_DUAL_8BIT_PAR_GEN_CHK_TOP
     AUTHOR : JoonSoo Ha

====================================*/
`timescale 1ns/1ps

module tb_DUAL_8BIT_PAR_GEN_CHK_TOP;


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
reg[7:0]  I_DAT1       ;
reg       I_PAR1       ;
reg[7:0]  I_DAT2       ; 
reg       I_PAR2       ; 
reg       I_CHK_GENN   ;

wire      O_ODD1N      ;
wire      O_ODD2N      ; 
wire      O_ERRN       ;


//=====================================
//
//          PORT MAPPING
//
//=====================================
// uud0
DUAL_8BIT_PAR_GEN_CHK_TOP uut0_DUAL_8BIT_PAR_GEN_CHK_TOP(
 	.I_DAT1     (I_DAT1    )  ,
	.I_PAR1     (I_PAR1    )  ,
	.I_DAT2     (I_DAT2    )  ,
	.I_PAR2     (I_PAR2    )  ,
	.I_CHK_GENN (I_CHK_GENN)  ,
                           
	.O_ODD1N    (O_ODD1N   )  ,
	.O_ODD2N    (O_ODD2N   )  ,
	.O_ERRN     (O_ERRN    )    
);


//=====================================
//
//          STIMULI
//
//=====================================
// input signal generation
reg[7:0] i;
initial begin
	$display("===== SIM START =====");
	// insert your code ...
	// ODD PARITY CHK 
	I_CHK_GENN = 1'b1; // CHECK
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0000,1'b0,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0000,1'b0,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0000,1'b1,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0000,1'b1,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0001,1'b0,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0001,1'b0,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0001,1'b1,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0001,1'b1,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz};
	#100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0000,1'b0,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0000,1'b0,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0000,1'b1,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0000,1'b1,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0001,1'b0,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0001,1'b0,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0001,1'b1,1'b0}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0001,1'b1,1'b1}; #100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;

	// ODD PARITY GEN
	I_CHK_GENN = 1'b0; // GEN
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0000,1'bx,1'bx}; #400;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0001,1'bx,1'bx}; #400;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0000,1'bx,1'bx}; #400;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0001,8'b0000_0001,1'bx,1'bx}; #400;
	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'bzzzz_zzzz,8'bzzzz_zzzz,1'bz,1'bz}; 
	#100;

	{I_DAT1,I_DAT2,I_PAR1,I_PAR2} = {8'b0000_0000,8'b0000_0000,1'b0,1'b0}; 
	$display("=====  SIM END  =====");
	$finish;
end

endmodule








