/*====================================

     MODULE : DUAL_8BIT_PAR_GEN_CHK_TOP
     AUTHOR : JoonSoo Ha

====================================*/
module DUAL_8BIT_PAR_GEN_CHK_TOP(
 	I_DAT1       ,
	I_PAR1       ,
	I_DAT2       ,
	I_PAR2       ,
	I_CHK_GENN   ,

	O_ODD1N      ,
	O_ODD2N      ,
	O_ERRN         
);


//=====================================
//
//          PARAMETERS 
//
//=====================================


//=====================================
//
//          I/O PORTS 
//
//=====================================
input[7:0]  I_DAT1       ;
input       I_PAR1       ;
input[7:0]  I_DAT2       ; 
input       I_PAR2       ; 
input       I_CHK_GENN   ;

output      O_ODD1N      ;
output      O_ODD2N      ; 
output      O_ERRN       ;


//=====================================
//
//          REGISTERS
//
//=====================================


//=====================================
//
//          WIRES
//
//=====================================
wire        w_odd1_par_n;
wire        w_odd2_par_n;


//=====================================
//
//          MAIN
//
//=====================================
assign      w_odd1_par_n = (^I_DAT1)^(~(I_PAR1&I_CHK_GENN));
assign      w_odd2_par_n = (^I_DAT2)^(~(I_PAR2&I_CHK_GENN));


//=====================================
//
//          OUTPUT ASSIGN
//
//=====================================
assign      O_ODD1N = w_odd1_par_n;
assign      O_ODD2N = w_odd2_par_n;
assign      O_ERRN  = ~(w_odd1_par_n|w_odd2_par_n);

endmodule











